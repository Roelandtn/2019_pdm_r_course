Download link: https://framagit.org/Roelandtn/2019_pdm_r_course/-/archive/master/2019_pdm_r_course-master.zip


# Prerequisites
## For the course

* R > 3.5
* tidyverse (dplyr and ggplot2 are included in the tidyverse)
* gapminder (data source)
* sf (spatial object handling)
* sp (spatial objects)
* spData (data source)

```R
install.packages(c("tidyverse", "sp", "sf", "spData"))
```

See this link for more information on how to install spatial libraries that sf needs on Ubuntu:
[rtask.thinkr.fr/installation-of-r-3-5-on-ubuntu-18-04-lts-and-tips-for-spatial-packages](https://rtask.thinkr.fr/installation-of-r-3-5-on-ubuntu-18-04-lts-and-tips-for-spatial-packages/)

## To produce the document
* Rmarkdown
* Xaringan (slideshow)
* TinyTeX (for pdf)
